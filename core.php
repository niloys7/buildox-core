<?php
/*
  Plugin Name: Buildox Core
  Description: Made For Buildox Theme
  Author: Codeixer
  Author URI: https://codeixer.com
  Version: 1.0
  Text Domain: fw
 */
 
/**
 *  Check if Unyson is active
 * @internal
 */
function cix_fw_active() {

	$active = false;

	$active_plugins = get_option( 'active_plugins' );

	if ( in_array( 'unyson/unyson.php', $active_plugins, true ) ) {
		$active = true;
	}

	if ( ! function_exists( 'is_plugin_active_for_network' ) ) {
		require_once( ABSPATH . '/wp-admin/includes/plugin.php' );
	}

	if ( is_plugin_active_for_network( 'unyson/unyson.php' ) ) {
		$active = true;
	}

	return $active;

}
function buildox_core_init(){

  if (cix_fw_active() == 'true') {
      
add_filter('fw_extensions_locations', 'cix_theme_filter__extensions');
add_action( 'widgets_init','buildox_widget_reg');
add_action('fw_init', '_cix_theme_widgets');
add_action( 'admin_enqueue_scripts', 'buildox_post_screen' );

  }else{
    add_action('admin_notices', 'buildox_unyson_nf'); 
     return;
  }
 

}

add_action('plugins_loaded', 'buildox_core_init');

function buildox_unyson_nf(){
  $plugin_data = get_plugin_data(__FILE__);
  echo '
  <div class="updated">
    <p>'.sprintf(__('<strong>%s</strong> requires <strong><a href="https://wordpress.org/plugins/unyson" target="_blank">Unyson</a></strong> plugin to be installed and activated on your site.', 'fw'), $plugin_data['Name']).'</p>
  </div>';
}


function cix_theme_filter__extensions($extensions) {

    $extensions[dirname(__FILE__) . '/extensions'] =  plugin_dir_url( __FILE__ ) . 'extensions';  

    return $extensions;

}


 function buildox_widget_reg(){
  register_widget( 'Widget_CixContactInfo' );
  register_widget( 'Widget_CixAboutUs' );
  register_widget( 'Widget_CixPhotoGallery' );
  register_widget( 'Widget_CixPostType' );
  register_widget( 'Widget_CixSocial' );
};

function _cix_theme_widgets(){
  
  require_once plugin_dir_path( __FILE__ ) .'widgets/contact-info/widget-contact-info.php';
  require_once plugin_dir_path( __FILE__ ) .'widgets/about-us/widget-about-us.php';
  require_once plugin_dir_path( __FILE__ ) .'widgets/photo-gallery/widget-photo-gallery.php';
  require_once plugin_dir_path( __FILE__ ) .'widgets/post-type/widget-post-type.php';
  require_once plugin_dir_path( __FILE__ ) .'widgets/social/widget-social.php';
}

// Condition for Unyson Active



function buildox_post_screen() {
		

	    $current_screen = get_current_screen();
	    if( $current_screen ->id === "post" ) {
			// Run some code, only on the Blog posts
			wp_add_inline_script( 'fw', 'jQuery(document).ready(function(){ 
				jQuery(".fw-backend-option-design-default").fadeOut();
				// Audio
				 
  
				if(jQuery("input#post-format-audio").is(":checked")) {	     		
						  jQuery("#fw-backend-option-fw-option-audio").fadeIn();
						  
				} 
				  jQuery("input#post-format-audio").click(function() {   
					jQuery(".fw-backend-option-design-default").fadeOut();
					jQuery("#fw-backend-option-fw-option-audio").fadeIn();
					
				  });
				// video
				if(jQuery("input#post-format-video").is(":checked")) {	     		
						  jQuery("#fw-backend-option-fw-option-video").fadeIn();
				} 
				  jQuery("input#post-format-video").click(function() {      
					jQuery(".fw-backend-option-design-default").fadeOut();
					jQuery("#fw-backend-option-fw-option-video").fadeIn();
					
				  });
				// gallery
				if(jQuery("input#post-format-gallery").is(":checked")) {	     		
						  jQuery("#fw-backend-option-fw-option-Gallery").fadeIn();
				} 
				  jQuery("input#post-format-gallery").click(function() {   
						jQuery(".fw-backend-option-design-default").fadeOut();
						  jQuery("#fw-backend-option-fw-option-Gallery").fadeIn();
  
					});
				// quote
  
			   if(jQuery("input#post-format-quote").is(":checked")) {	     		
						  jQuery("#fw-backend-option-fw-option-Quote").fadeIn();
				}  
				  jQuery("input#post-format-quote").click(function() {  
   
						jQuery(".fw-backend-option-design-default").fadeOut();
						  jQuery("#fw-backend-option-fw-option-Quote").fadeIn();
					
				  });
			  });' );
	        
	    }    
	    
}

