<?php echo $before_widget; ?>

<div class="photo-gallery ul-li clearfix">
	
	<?php echo $before_title.'' .esc_html( $instance['title'] ).''. $after_title;  ?>
	
	
	<ul class="clearfix">
	<?php 
		foreach ($instance['pg'] as $img) { ?>
			<li>
			<div class="image-container mfpp">
				
				<?php $img_url = wp_get_attachment_image_src( $img['attachment_id'], 'thumbnail' ); ?>
				<img src="<?php echo esc_url( $img_url[0] ); ?>" alt="Photo">
				<a href="<?php echo esc_url( $img['url'] ); ?>" class="plus-effect"></a>
			</div>
		</li>
		<?php }
	?>
		
	</ul>
</div>

<?php echo $after_widget; ?>