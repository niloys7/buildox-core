<?php

/**
 * Disable unused unyson shortcodes
 **/

function _filter_wplab_recover_disable_unyson_shortcodes( $to_disable ) {
	$to_disable[] = 'contact_form';
	$to_disable[] = 'calendar';
	//$to_disable[] = 'call_to_action';
	//$to_disable[] = 'icon';
	//$to_disable[] = 'icon_box';
//	$to_disable[] = 'blog_view';
	
	return $to_disable;
}

add_filter( 'fw_ext_shortcodes_disable_shortcodes', '_filter_wplab_recover_disable_unyson_shortcodes' );