<?php if (!defined('FW')) die('Forbidden');


//fw_print($field);
$options = array(
	'slider_alias' => array(
		'label' => esc_html__( 'Choose a slider', 'fw' ),
		'type' => 'select',
		'value' => '',
		'choices' => buildox_revolution_slides(),
		'desc' => esc_html__( 'Select one of created sliders', 'fw' ),
	),
);
