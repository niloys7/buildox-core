<?php if (!defined('FW')) die('Forbidden');

if (!function_exists('_action_theme_shortcode_table_btn_enqueue_dynamic_css')):

    /**
     * @internal
     * @param array $data
     */
    function _action_theme_shortcode_table_btn_enqueue_dynamic_css($data) {
        $shortcode = 'table_btn';
        $atts = shortcode_parse_atts( $data['atts_string'] );
        $atts = fw_ext_shortcodes_decode_attr($atts, $shortcode, $data['post']->ID);

        wp_add_inline_style(
            'buildox-core',
            '.shortcode-'. $atts['id'] .' .pricing-plan-item:hover .bg-default-orange{ '.
                'color: '. $atts['color'] .';'.
            ' } 
            
            '
            
           
            
        );
    }
    add_action(
        'fw_ext_shortcodes_enqueue_static:table_btn',
        '_action_theme_shortcode_table_btn_enqueue_dynamic_css'
    );
    
    endif;