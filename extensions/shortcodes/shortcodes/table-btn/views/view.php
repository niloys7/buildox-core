<?php if (!defined('FW')) die( 'Forbidden' ); 
$id = uniqid('table-btn-');
?>


<a href="<?php echo esc_attr($atts['link']) ?>" target="<?php echo esc_attr($atts['target']) ?>" class="custom-btn <?php echo esc_attr('shortcode-' . $atts['id']); ?>">
	<span class="btn-bg bg-default-orange" style="border: 2px solid <?php echo esc_attr($atts['color']); ?>;background-color: <?php echo esc_attr($atts['color']); ?>;"><i class="fa fa-shopping-cart"></i>
		<?php echo $atts['label']; ?>
	</span>
</a>
