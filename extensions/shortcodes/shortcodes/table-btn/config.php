<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg['page_builder'] = array(
	'title'       => __( 'Pricing Button', 'fw' ),
	'description' => __( 'Add a Pricing Button', 'fw' ),
	'tab'         => __( 'Hidden Elements', 'fw' ),
	'popup_size' => 'small'
);
