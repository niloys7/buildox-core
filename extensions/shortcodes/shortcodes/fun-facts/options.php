<?php if ( ! defined( 'FW' )  ) {
	die( 'Forbidden' );
}

$options = array(
	'id'    => array( 'type' => 'unique' ),
	'tab1' => array(
		'title' => esc_html__('General', 'fw'),
		'type' => 'tab',
		'options' => array(
			'icon'    => array(
			'type'  => 'icon-v2',
			'label' => __('Choose an Icon', 'fw'),
		),

		'number'   => array(
			'type'  => 'short-text',
			'label' => __( 'Number', 'fw' ),
		 	'value' => '100',
		), 
		'title'   => array(
			'type'  => 'text',
			'label' => __( 'Title of the Box', 'fw' ),
		 	'value' => 'Title',
		), 
			
	 		
		),
	),
	'tab2' => array(
		'title' => esc_html__('Style', 'fw'),
		'type' => 'tab',
		'options' => array(
			'icon_color' => array(
			    'type'  => 'color-picker',
			    'value' => '#ff4d1c',
			    
			    // palette colors array
			    'palettes' => array( '#f7c605', '#ff4d1c','#009326','#006cff','#9227ff','#0cff84'),
			    'label' => __('Icon Color', 'fw'),
			    
			),
			'num_color' => array(
			    'type'  => 'color-picker',
			    'value' => '#fff',
			    
			    // palette colors array
			    'palettes' => array(),
			    'label' => __('Number Color', 'fw'),
			    
			),
			'title_color' => array(
			    'type'  => 'color-picker',
			    'value' => '#a8a8a8',
			    
			    // palette colors array
			    'palettes' => array(),
			    'label' => __('Title Color', 'fw'),
			    
			),
	 		
		),
	),
);