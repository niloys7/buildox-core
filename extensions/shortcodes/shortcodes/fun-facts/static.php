<?php if (!defined('FW')) die('Forbidden');

// find the uri to the shortcode folder
$uri = fw_get_framework_directory('/extensions/shortcodes/shortcodes/fun-facts');
$uri = plugin_dir_url( __FILE__ );

wp_enqueue_script(
    'waypoints',
    $uri . 'static/js/waypoints.min.js'
);
wp_enqueue_script(
    'counterup',
    $uri . 'static/js/jquery.counterup.min.js'
);

wp_enqueue_script(
    'counterup-scripts',
    $uri . '/static/js/script.js'
);

if (!function_exists('_action_theme_shortcode_fun_facts_enqueue_dynamic_css')):

    /**
     * @internal
     * @param array $data
     */
    function _action_theme_shortcode_fun_facts_enqueue_dynamic_css($data) {
        $shortcode = 'fun_facts';
        $atts = shortcode_parse_atts( $data['atts_string'] );
        $atts = fw_ext_shortcodes_decode_attr($atts, $shortcode, $data['post']->ID);

        wp_add_inline_style(
            'buildox-core',
            '.shortcode-'. $atts['id'] .' i { '.
                'color: '. $atts['icon_color'] .';'.
            ' } 
            .shortcode-'. $atts['id'] .' .counter-item .counter-text { '.
                'color: '. $atts['num_color'] .';'.
                
            ' }
            .shortcode-'. $atts['id'] .' .counter-item .counter-title { '.
                'color: '. $atts['title_color'] .';'.
                
            ' }
            '
            
            
            
        );
    }
    add_action(
        'fw_ext_shortcodes_enqueue_static:fun_facts',
        '_action_theme_shortcode_fun_facts_enqueue_dynamic_css'
    );
    
    endif;