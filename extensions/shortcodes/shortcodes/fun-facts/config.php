<?php if ( ! defined( 'FW' )  ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg['page_builder'] = array(
	'title'       => __( 'Fun Fact', 'fw' ),
	'description' => __( 'Fun Fact', 'fw' ),
	'tab'         => __( 'Theme Elements', 'fw' ),
	'icon' => 'fa fa-cubes',
	 'title_template' => '{{- title }} <span class="cix-title-template">{{=o.number}} <br> {{- o.title}} </span>',
);