<?php if (!defined('FW')) die( 'Forbidden' ); ?>

<?php
/*
 * the `.fw-tabs-container` div also supports
 * a `tabs-justified` class, that strethes the tabs
 * to the width of the whole container
 */
// fw_print($atts);
?>
<div class="funfact-section <?php echo esc_attr('shortcode-' . $atts['id']); ?>">
	<div class="counter-item text-center">
		<span class="icon color-default-yellow mb-15"><i class="<?php echo esc_attr( $atts['icon']['icon-class'] ); ?>"></i></span>
		<h2 class="counter-text mb-15"><?php echo esc_html( $atts['number'] ); ?></h2>
		<small class="counter-title"><?php echo esc_html( $atts['title'] ); ?></small>
	</div>
</div>
