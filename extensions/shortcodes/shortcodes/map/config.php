<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg['page_builder'] = array(
	
	'disable_correction' => true,
	'title'       => __( 'Map', 'fw' ),
	'description' => __( 'Add a Map', 'fw' ),
	'tab'         => __( 'Content Elements', 'fw' ),
	 
);