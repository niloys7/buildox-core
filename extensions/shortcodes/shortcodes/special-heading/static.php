<?php if (!defined('FW')) die('Forbidden');


wp_dequeue_script('fw-shortcode-special-heading');


if (!function_exists('_action_theme_shortcode_special_heading_enqueue_dynamic_css')):

    /**
     * @internal
     * @param array $data
     */
    function _action_theme_shortcode_special_heading_enqueue_dynamic_css($data) {
        $shortcode = 'special_heading';
        $atts = shortcode_parse_atts( $data['atts_string'] );
        $atts = fw_ext_shortcodes_decode_attr($atts, $shortcode, $data['post']->ID);
       
$font_size = ($atts['font_size']) ? 'font-size:'. esc_attr($atts['font_size']) .'px;' : '';

        wp_add_inline_style(
            'buildox-core',
            '.shortcode-'.$atts['id'].' .sec-title-text { '.
                'color: '. $atts['h_color'] .';'.
            ' } 
            .shortcode-'. $atts['id'] .' .sec-subtitle-text { '.
                'color: '. $atts['sub_color'] .';'.
                ''.$font_size .' '. 
            ' }
            '
            
            
            
        );
    }
    add_action(
        'fw_ext_shortcodes_enqueue_static:special_heading',
        '_action_theme_shortcode_special_heading_enqueue_dynamic_css'
    );
    
    endif;