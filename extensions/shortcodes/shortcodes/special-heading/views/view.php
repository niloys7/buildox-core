<?php if (!defined('FW')) {
	die('Forbidden');
}

/**
 * @var $atts
 */
//fw_print($atts['id']);

?>

<div style="margin-bottom:<?php echo esc_attr($atts['m_bottom']); ?>" class="spa-heading section-title  <?php echo esc_attr($atts['align'] . ' shortcode-' . $atts['id']); ?>">
	<?php if (!empty($atts['subtitle'])): ?>
		<span class="sec-subtitle-text mb-30">
			<small class="line-design line-design-left"></small>
			<?php echo $atts['subtitle']; ?>
			<?php if ($atts['style'] == 'value-2'): ?>

				<small class="line-design line-design-right"></small>

			<?php endif;?>
		</span>
	<?php endif;?>
	<?php $heading = "<{$atts['heading']} class='sec-title-text'>{$atts['title']}</{$atts['heading']}>";?>
	<?php echo $heading; ?>

</div>
