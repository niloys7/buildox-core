<?php if (!defined('FW')) {
	die('Forbidden');
}
/*
'tab2' => array(
'title' => esc_html__('Extra options', 'fw'),
'type' => 'tab',
'options' => array(

),
),

'align' => array(
'type'  => 'select',
'value' => 'text-center',

'label' => __('Text Align', 'fw'),
'choices' => array(
'' => '---',
'text-left' => __('Text Left', 'fw'),
'text-center' => __('Text Center', 'fw'),
'text-right' => __('Text Right', 'fw'),

),

),
 */
$options = array(
	'id'    => array( 'type' => 'unique' ),
	'tab1' => array(
		'title' => esc_html__('General', 'fw'),
		'type' => 'tab',
		'options' => array(
			'title' => array(
				'type' => 'text',
				'label' => __('Heading Title', 'fw'),
				'desc' => __('Write the heading title content', 'fw'),
			),
			'subtitle' => array(
				'type' => 'text',
				'label' => __('Heading Subtitle', 'fw'),
				'desc' => __('Write the heading subtitle content', 'fw'),
			),

		),
	),
	'tab2' => array(
		'title' => esc_html__('Style', 'fw'),
		'type' => 'tab',
		'options' => array(

			'style' => array(
				'type' => 'image-picker',
				'value' => 'value-1',

				'label' => __('Sub Title Style', 'fw'),

				'choices' => array(
					'value-1' => array(
						'small' => array(
							'src' => BUILDOX_IMAGES . '/wp/sub_1.png',
							'height' => 20,

						),

					),
					'value-2' => array(
						'small' => array(
							'src' => BUILDOX_IMAGES . '/wp/sub_2.png',
							'height' => 20,
						),

					),

				),
				'blank' => false, // (optional) if true, images can be deselected
			),

			'heading' => array(
				'type' => 'select',
				'label' => __('Heading Size', 'fw'),
				'value' => 'h2',
				'choices' => array(
					'h1' => 'H1',
					'h2' => 'H2',
					'h3' => 'H3',
					'h4' => 'H4',
					'h5' => 'H5',
					'h6' => 'H6',
				),
			),
			'align' => array(
				'type' => 'select',
				'value' => '',

				'label' => __('Heading Text Align', 'fw'),
				'choices' => array(
					'' => '--',
					'text-left' => __('Text Left', 'fw'),
					'text-center' => __('Text Center', 'fw'),
					'text-right' => __('Text Right', 'fw'),

				),

			),
			'font_size' => array(
				'type' => 'short-text',
				'label' => __('Font Size', 'fw'),

			),
			'm_bottom' => array(
				'type' => 'short-text',
				'label' => __('Margin Bottom', 'fw'),
				'value' => '30px',
			),
			'h_color' => array(
				'type' => 'color-picker',
				'value' => '#0d0e25',

				// palette colors array
				'palettes' => array('#f7c605', '#ff4d1c', '#009326', '#006cff', '#9227ff', '#0cff84'),
				'label' => __('Heading Color', 'fw'),

			),
			'sub_color' => array(
				'type' => 'color-picker',
				'value' => '#ff4d1c',

				// palette colors array
				'palettes' => array('#f7c605', '#ff4d1c', '#009326', '#006cff', '#9227ff', '#0cff84'),
				'label' => __('Sub Title Color', 'fw'),

			),

		),
	),
);
