<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => __('Special Heading', 'fw'),
	'description'   => __('Add a Special Heading', 'fw'),
	'tab'           => __('Theme Elements', 'fw'),
	'icon' => 'fa fa-header',
	 'title_template' => '{{- title }} <span class="cix-title-template">Title : {{=o.title}} <br>
	 {{ if (o.subtitle) { }} SubTitle : {{-o.subtitle}}<br> {{ } }} 
	
	  </span>',
);