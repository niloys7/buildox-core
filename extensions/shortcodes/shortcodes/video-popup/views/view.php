<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
//fw_print($atts);

?>
<div class="intro-section">
<div class="video-item image-container">
	<a href="<?php echo $atts['video']; ?>" class="video-link popup-youtube">
		<img src="<?php echo $atts['image']['url']; ?>" alt="Video Image">
		<span class="icon"><i class="flaticon-play-button-1"></i></span>
	</a>
</div>
</div>