<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(

	'image' => array(
			'label' => __( 'Video Image', 'fw' ),
			'desc'  => __( 'Either upload a new, or choose an existing image from your media library', 'fw' ),
			'type'  => 'upload'
		),
		'video'  => array(
			'label' => __( 'Video Link', 'fw' ),
			'desc'  => __( 'Add Video Url Here', 'fw' ),
			'type'  => 'text',
			'value' => ''
		),
	


	
	
);