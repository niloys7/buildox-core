<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'tabs' => array(
		'type'          => 'addable-popup',
		'label'         => __( 'Icons', 'fw' ),
		'popup-title'   => __( 'Add/Edit Icons', 'fw' ),
		'desc'          => __( 'Create Icons', 'fw' ),
		'template'      => '{{=title}}',
		'popup-options' => fw_ext('shortcodes')->get_shortcode('icon_box')->get_options()
	),
	

	'cf7_title' => array(
		'type'          => 'text',
		'label'         => __( 'From Title', 'fw' ),
		'desc'          => __( 'Add From Title', 'fw' ),

	),
	'cf7_id' => array(
		'label' => esc_html__( 'Choose a From', 'fw' ),
		'type' => 'multi-select',
		
		'population' => 'posts',
		'source' => 'wpcf7_contact_form',
		'limit' => 1,
		'desc' => esc_html__( 'Select one of created Forms', 'fw' ),
	),
				
);
