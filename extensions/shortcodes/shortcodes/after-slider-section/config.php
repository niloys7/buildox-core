<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg['page_builder'] = array(
	'disable_correction' => true,
	'title'       => __( 'After Slider', 'fw' ),
	'description' => __( 'Add After Slider Section', 'fw' ),
	'tab'         => __( 'Theme Elements', 'fw' ),
);