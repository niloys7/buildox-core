<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
?>
<div class="feature-section">
			
				<div class="row justify-content-lg-start justify-content-md-center">

					<div class="col-lg-7 col-md-10 col-sm-12">
						<div class="row">
							<?php foreach ($atts['tabs'] as $icon) { ?>
							<div class="col-lg-6 col-md-6 col-sm-12">
								<div class="feature-item-2 clearfix">
									<span class="icon color-default-yellow"><i style="color:<?php echo $icon['icon_color'];?>" class="<?php echo $icon['icon']['icon-class'];?>"></i></span>
									<div class="feature-content">
										<h3 class="feature-title mb-15"><?php echo $icon['title'];?></h3>
										<p class="mb-20">
											<?php echo $icon['content'];?>
										</p>
										<?php if($icon['link_txt']) : ?>
										<a href="<?php echo $icon['link_url'];?>" class="details-btn"><?php echo $icon['link_txt'];?></a>
									<?php endif; ?>
									</div>
								</div>
							</div>

							<?php } ?>

							

							
						</div>
					</div>
					<div class="col-lg-5 col-md-8 col-sm-12">
						<div class="quote-form text-center">
							<div class="form-title bg-default-orange">
								<h2 class="title-text">
									<?php echo $atts['cf7_title']; ?>
								</h2>
							</div>
							<!-- cf7 -->
							<?php echo do_shortcode( '[contact-form-7 id="'.$atts['cf7_id']['0'].'" ]' ); ?>
						</div>
					</div>
					
				</div>
			
</div>

<script>
  jQuery('body').addClass('home-page-2');
</script>