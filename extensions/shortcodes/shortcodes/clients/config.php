<?php if ( ! defined( 'FW' )  ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg['page_builder'] = array(
	'title'       => __( 'Client', 'fw' ),
	'description' => __( 'Add Clients Logo', 'fw' ),
	'tab'         => __( 'Theme Elements', 'fw' ),

);