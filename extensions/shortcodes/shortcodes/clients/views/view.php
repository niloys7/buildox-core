<?php if ( ! defined( 'FW' )  ) {
	die( 'Forbidden' );
} ?>

 <!-- Our Awards -->
<?php  

$logos = $atts['logos'];
//fw_print($logos);
?>
<div class="client-section">

            <div class="client-logo-list ul-li clearfix">
              <ul class="clearfix">
                <?php foreach ($logos as $logo) {
                  # code...
                  echo  '<li><a href="#!"><img src="'.$logo['url'].'" alt="'.$logo['attachment_id'].'"></a></li>';
                } ?>
               
              </ul>
            </div>

          </div>