<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
	'page_builder' => array(
		'title'       => esc_html__( 'Contact Form', 'fw' ),
		'description' => esc_html__( 'Add Contact Form', 'fw' ),
		'tab'         => esc_html__( 'Theme Elements', 'fw' ),
		'icon'		=> 'fa fa-envelope-open-text',
		
		
	)
);
