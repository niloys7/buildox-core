<?php if (!defined('FW')) die('Forbidden');

//fw_print($field);

$options = array(
	'cf7_id' => array(
		'label' => esc_html__( 'Choose a From', 'fw' ),
		'type' => 'multi-select',
		
		'population' => 'posts',
		'source' => 'wpcf7_contact_form',
		'limit' => 1,
		'desc' => esc_html__( 'Select one of created Forms', 'fw' ),

        
	),
);