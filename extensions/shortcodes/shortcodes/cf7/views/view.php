<?php if (!defined('FW')) die('Forbidden');

/**
 * @var $atts The shortcode attributes
 */


echo '<div class="buildox-cf7-container">';
echo do_shortcode( '[contact-form-7 id="'.$atts['cf7_id']['0'].'" ]' );
echo '</div>';

