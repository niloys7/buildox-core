<?php if (!defined('FW')) die('Forbidden');

$options = array(
	'id' => array( 'type' => 'unique' ),
	'general' => array(
		'title' => esc_html__( 'General', 'fw' ),
		'type' => 'tab',
		'options' => array(
		
			'style' => array(
				'label' => esc_html__( 'Output style', 'fw' ),
				'type' => 'select',
				'value' => '',
				'choices' => array(
					'list' => esc_html__('List', 'fw'),
					'grid_cols_2' => esc_html__('Grid, 2 columns', 'fw'),
					'grid_cols_3' => esc_html__('Grid, 3 columns', 'fw'),
					'masonry_cols_2' => esc_html__('Masonry, 2 columns', 'fw'),
					'masonry_cols_3' => esc_html__('Masonry, 3 columns', 'fw'),
					
				),
			),
			'pagination' => array(
				'type' => 'multi-picker',
				'label' => false,
				'desc' => false,
				'picker' => array(
					'enabled' => array(
						'label' => esc_html__( 'Display pagination', 'fw' ),
						'type' => 'switch',
						'left-choice' => array(
							'value' => 'false',
							'color' => '#ccc',
							'label' => esc_html__( 'No', 'fw' )
						),
						'right-choice' => array(
							'value' => 'true',
							'label' => esc_html__( 'Yes', 'fw' )
						),
						'value' => 'true',
					)
				),
				'choices' => array(
					'true' => array(

						'pagination_style' => array(
							'label' => esc_html__( 'Pagination style', 'fw' ),
							'type' => 'select',
							'value' => '',
							'choices' => array(
								//'prev_next' => esc_html__( 'Prev / Next links', 'fw'),
								'number' => esc_html__( 'Numbers', 'fw'),
								'ajax' => esc_html__( 'AJAX', 'fw'),
							),
						),
						'load_more' => array(
							'label' => esc_html__( 'Load More', 'fw' ),
							'desc' => esc_html__( 'Change load more text', 'fw' ),
							'type' => 'text',
							'value' => 'Load More',
						),

					),
				),
				'show_borders' => false,
			),
		
		)
	),
	'query' => array(
		'title' => esc_html__( 'Query', 'fw' ),
		'type' => 'tab',
		'options' => array(
			'posts_per_page' => array(
				'label' => esc_html__( 'Posts per page', 'fw' ),
				'type' => 'text',
				'value' => '9'
			),
			'order_by' => array(
				'label' => esc_html__( 'Posts ordering method', 'fw' ),
				'type' => 'select',
				'value' => '',
				'choices' => array(
					'date' => esc_html__('Date', 'fw' ),
					'ID' => 'ID',
					'modified' => esc_html__('Modified date', 'fw' ),
					'title' => esc_html__('Title', 'fw'),
					'rand' => esc_html__('Random', 'fw'),
					'menu' => esc_html__('Menu', 'fw')
				),
			),
			'sort_by' => array(
				'label' => esc_html__( 'Posts sorting method', 'fw' ),
				'type' => 'select',
				'value' => '',
				'choices' => array(
					'DESC' => esc_html__('Descending', 'fw'),
					'ASC' => esc_html__('Ascending', 'fw'),
				),
			),
			'taxonomy_query' => array(
				'type' => 'multi-picker',
				'label' => false,
				'desc' => false,
				'value' => array(
					'tax_query_type' => '',
				),
				'picker' => array(
					'tax_query_type' => array(
						'label' => esc_html__( 'Query from category', 'fw' ),
						'type' => 'radio',
						'choices' => array(
							'' => esc_html__( 'All', 'fw' ),
							'only' => esc_html__( 'Only', 'fw' ),
							'except' => esc_html__( 'Except', 'fw' ),
						),
					)
				),
				'choices' => array(
					'only' => array(
					
						'cats_include' => array(
							'label' => esc_html__('Categories', 'fw'),
							'desc' => esc_html__('Select the categories to include or exclude, based on previous parameter.', 'fw'),
							'type' => 'multi-select',							
							'population' => 'taxonomy',
							'source' => 'category',
						),
					
					),
					'except' => array(
					
						'cats_exclude' => array(
							'label' => esc_html__('Categories', 'fw'),
							'desc' => esc_html__('Select the categories to include or exclude, based on previous parameter.', 'fw'),
							'type' => 'multi-select',							
							'population' => 'taxonomy',
							'source' => 'category',
						),
					
					),
				)
			),
			'with_thumbs_only' => array(
				'label' => esc_html__( 'Posts with thumbnails only', 'fw' ),
				'type' => 'switch',
				'left-choice' => array(
					'value' => 'false',
					'color' => '#ccc',
					'label' => esc_html__( 'No', 'fw' )
				),
				'right-choice' => array(
					'value' => 'true',
					'label' => esc_html__( 'Yes', 'fw' )
				),
				'value' => 'false',
			),
		)
	),
);