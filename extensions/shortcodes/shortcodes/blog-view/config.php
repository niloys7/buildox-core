<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
	'page_builder' => array(
		'disable_correction' => true,
		'title'       => esc_html__( 'Blog View', 'fw' ),
		'description' => esc_html__( 'List of posts', 'fw' ),
		'tab'         => esc_html__( 'Theme Elements', 'fw' ),
	)
);