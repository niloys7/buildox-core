<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
/**
 * @var array $atts
 */
$id = uniqid('shortcode-');
//fw_print($atts);
$image_src = fw_resize( fw_akg('img_src/attachment_id', $atts), '491', '559', true ); 
$image_singnature = fw_resize( fw_akg('img_signature/attachment_id', $atts), '235', '135', true ); 

?>

<div class="manager-section">
<div class="manager-image clearfix">
	<div class="image-container float-right">
		<img src="<?php echo esc_url($image_src); ?>" alt="Founder Image">
	</div>
	<span class="signature-image">
		<img src="<?php echo esc_url($image_singnature); ?>" alt="Founder Sign">
	</span>
</div>
</div>