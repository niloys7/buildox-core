<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => __('Founder Image', 'fw'),
	'description'   => __('Add an Image Box', 'fw'),
	'tab'           => __('Theme Elements', 'fw'),
	'icon' => 'fa fa-user-circle',
);