<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
//fw_print(fw_ext('shortcodes'));
$options = array(

	'img_src' => array(
				'type'          => 'upload',
				'label' => __('Image', 'fw'),
				'images_only' => true,
				
			),
	'img_signature' => array(
				'type'          => 'upload',
				'label' => __('Signature', 'fw'),
				'images_only' => true,
				
			)
							
						

	
	/*
	'tab2' => array(
		'title' => esc_html__('Extra options', 'fw'),
		'type' => 'tab',
		'options' => array(
	 		
		),
	),


	'button' => array(
				'type'          => 'popup',
				'popup-title'   => __( 'Add New Image', 'fw' ),
				'button'        => __( 'Add', 'fw' ),
				'popup-options' => fw_ext('shortcodes')->get_shortcode('media_image')->get_options()
			)


	*/

	
);