<?php if (!defined('FW')) die( 'Forbidden' ); ?>
<?php $color_class = !empty($atts['color']) ? "{$atts['color']}" : ''; ?>

<a href="<?php echo esc_attr($atts['link']) ?>" target="<?php echo esc_attr($atts['target']) ?>"  class="custom-btn ">
	<span class="btn-bg  <?php echo esc_attr($color_class); ?>"><?php echo $atts['label']; ?></span>
</a>