<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
//fw_print($atts);

// The Query
$args = array(
 			'post_type' => 'fw-portfolio',
 			'order' => $atts['ordr'],  // ASC
 			'posts_per_page' => $atts['ppp'], 
 			'cat' => $atts['cat'],//(int) - use category id.'cat' => 5,//(int) - use category id.
);
$the_query = new WP_Query( $args );

// The Loop
if ( $the_query->have_posts() ) {  
	if($atts['style'] == '1') : 
?>
<!-- Style 1 -->
<div class=" clearfix">
	<div class="container">

		<div class="section-title mb-30 text-center">

			<span class="sec-subtitle-text mb-30">
			<small class="line-design line-design-left"></small>
				<?php echo esc_html($atts['sub_title']);?>				
			<small class="line-design line-design-right"></small>
			</span>
			<h2 class="sec-title-text"><?php echo esc_html($atts['title']);?></h2>
		</div>

			<div class="row">
				<?php
	while ( $the_query->have_posts() ) {
		$the_query->the_post(); ?>
		
			<div class="fw-col-sm-6 fw-col-md-4">
					<div class="item">
						<div class="casestudy-grid-item">

							<div class="casestudy-image image-container">
								<?php the_post_thumbnail( $size = 'buildox-portfolio-thumb');?>
								<a href="<?php the_permalink(); ?>" class="plus-effect"></a>
							</div>

							<div class="casestudy-content">
								
							<?php 

								$terms = get_the_terms( get_the_ID(), 'fw-portfolio-category' );
											foreach ( $terms as $term ) {?>
											<a href="<?php echo esc_url(get_term_link( $term, $taxonomy = 'fw-portfolio-category' )); ?>" class="post-category">

											<?php echo esc_html( $term->name ); ?>
											</a>
											<?php }
										?>

							
								<a href="<?php the_permalink(); ?>" class="casestudy-title mb-30"><h3><?php the_title();?></h3></a>
								
								
								
								<?php
								echo the_excerpt();
												?>
													
											
								
								<a href="<?php the_permalink(); ?>" class="details-btn">
									<?php echo esc_html__( 'view case study', 'fw' ); ?>
								</a>
							</div>

						</div>
					</div>
				</div>   
						


	<?php } ?>
			 
				
			</div>
			
			
		
		
	</div>
</div>
<!-- Style 1 End -->
<?php else: ?>
<div class="row portfolio-section clearfix">
	<div class="container section-title mb-60 text-center">
		<span class="sec-subtitle-text mb-30">
		<small class="line-design line-design-left"></small>
			<?php echo esc_html($atts['sub_title']);?>
		<small class="line-design line-design-right"></small>
		</span>
		<h2 class="sec-title-text"><?php echo esc_html($atts['title']);?></h2>
	</div>
	<div class="portfolio-items-list ul-li clearfix">
		<ul class="clearfix">	

	<?php
	while ( $the_query->have_posts() ) {
		$the_query->the_post(); ?>
		
		<li>
				<a href="<?php the_permalink(); ?>" class="details-link">
					<?php the_post_thumbnail('buildox-portfolio-thumb-400'); ?>
				</a>
				<ul class="action-btns-group clearfix">
					<!-- <li><a href="<?php // the_permalink(); ?>"><i class="flaticon-magnifying-glass"></i></a></li> -->
					<li><a href="<?php the_permalink(); ?>"><i class="fa fa-link"></i></a></li>
				</ul>
			</li>
		


	<?php }
	echo '	</ul></div></div>';
	/* Restore original Post Data */
	wp_reset_postdata();

endif; // style1 condition end
} else {
	// no posts found
	 esc_html_e( 'Sorry, no posts matched your criteria.' ); 
}

?>
