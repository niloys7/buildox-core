<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg['page_builder'] = array(
	'disable_correction' => true,
	//'disable_columns_auto_wrap' => true,
	'title'       => __( 'Projects', 'fw' ),
	'description' => __( 'Add Projects', 'fw' ),
	'tab'         => __( 'Theme Elements', 'fw' ),
	'popup_size'  => 'medium',
	'icon' => 'fa fa-folder-open',
);