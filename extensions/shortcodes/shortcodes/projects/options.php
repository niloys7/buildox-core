<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'general' => array(
	'title' => esc_html__('General', 'fw'),
    'type' => 'tab',
    'options' => array(
		
		'sub-title'  => array(
			'label' => __( 'Sub Title', 'fw' ),
			
			'type'  => 'text',
			'value' => 'Protfolio'
		),
		'title'   => array(
			'label' => __( 'Title', 'fw' ),
			'desc'  => __( 'Project title', 'fw' ),
			'type'  => 'text',
			'value' => 'Latest projects.'
		),
		'style'   => array(
		    'type'  => 'image-picker',
		    'value' => '2',
		    
		    'label' => __('Layout', 'fw'),
		    
		    'choices' => array(
		        '1' => array(
		        	'small' => array(
		                'src' => BUILDOX_IMAGES .'/wp/p_1.png',
		                'width' => 200
		                
		            ),
		        	
	        	),
		        '2' => array(
		        	'small' => array(
		                'src' => BUILDOX_IMAGES .'/wp/p_2.png',
		                'width' => 200
		            ),
		        	
	        	),
		       
		        
		       
		    ),
		    'blank' => false, // (optional) if true, images can be deselected
		),
	),
	


	),
	'query_box' => array(
	'title' => esc_html__('Project Query', 'fw'),
    'type' => 'tab',
    'options' => array(
		 
            'ppp' => array(
                'type' => 'short-text',
                'value' => '4',
                'label' => __('Post Count:', 'fw'),
            ),
            'cat' => array(
                'type'  => 'multi-select',
                'label' => __('Categories:', 'fw'),
                'population' => 'taxonomy',
                'source' => 'fw-portfolio-category',
                'desc'=> __('Display Posts only from selected category', 'fw'),
            ),
            'ordr' => array(
                 'type'  => 'select',
                 'value' => 'DESC',
                 'label' => __('Post Order:', 'fw'),
                 'choices' => array(
                    'DESC' => 'DESC',
                    'ASC' => 'ASC',
                 ),
              
	),
	),
	


	),

	
	
);