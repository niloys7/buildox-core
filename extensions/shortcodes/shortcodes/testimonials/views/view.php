<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$id = uniqid( 'testimonials-' );
?>
<div class="testimonial-section">
<div class="testimonial-carousel owl-carousel owl-theme">
	<?php foreach ( fw_akg( 'testimonials', $atts, array() ) as $testimonial ): ?>
			<div class="item">
				<p class="mb-30">
					<?php echo $testimonial['content']; ?>
				</p>
				<div class="testimonial-hero">
					<?php
						$author_image_url = !empty($testimonial['author_avatar']['url'])
											? $testimonial['author_avatar']['url']
											: fw_get_framework_directory_uri('/static/img/no-image.png');
						?>
					<span class="hero-image float-left">
						<img src="<?php echo esc_attr($author_image_url); ?>" alt="<?php echo esc_attr($testimonial['author_name']); ?>"/>
					</span>
					<div class="hero-info">
						<h3 class="hero-name"><?php echo $testimonial['author_name']; ?></h3>
						<small class="hero-title"><?php echo $testimonial['author_job']; ?></small>
					</div>
				</div>
			</div>
	<?php endforeach; ?>
					
</div>
</div>