<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
/**
 * @var array $atts
 */
$id = uniqid('shortcode-');
// fw_print($id);


// fw_print(buildox_hex2rgba($atts['icon_color'],'0.1'));


/*
 * `.fw-iconbox` supports 3 styles:
 * `fw-iconbox-1`, `fw-iconbox-2` and `fw-iconbox-3`
 */
?>
<div class="contact-info-item text-center <?php echo esc_attr($id); ?>">
	<span class="icon bg-default-yellow" style="background-color: <?php echo esc_attr($atts['icon_bg']); ?>;">
		<i style="color: <?php echo esc_attr($atts['icon_color']); ?>;" class="<?php echo esc_attr($atts['icon']['icon-class']); ?>"></i>
	</span>
	<h2 class="title-text mb-30"><?php echo esc_html( $atts['title']); ?></h2>
	<div class="info-list ul-li clearfix">
		<?php echo $atts['content']; ?>
	</div>
</div>

