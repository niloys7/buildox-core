<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => __('Contact Info', 'fw'),
	'description'   => __('Add an Contact Info Icon Box', 'fw'),
	'tab'           => __('Theme Elements', 'fw'),
	'icon' => 'fa fa-info-circle',
	 'title_template' => '{{- title }} <span class="cix-title-template">{{=o.title}} <br>  </span>',
);