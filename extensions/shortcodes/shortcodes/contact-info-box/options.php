<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	
		'icon'    => array(
			'type'  => 'icon-v2',
			'label' => __('Choose an Icon', 'fw'),
		),
		'icon_color' => array(
			    'type'  => 'color-picker',
			    'value' => '#fff',
			    
			    // palette colors array
			    'palettes' => array( '#f7c605', '#ff4d1c','#009326','#006cff','#9227ff','#0cff84'),
			    'label' => __('Icon Color', 'fw'),
			    
			),
		'icon_bg' => array(
			    'type'  => 'color-picker',
			    'value' => '#f7c605',
			    
			    // palette colors array
			    'palettes' => array( '#f7c605', '#ff4d1c','#009326','#006cff','#9227ff','#0cff84'),
			    'label' => __('Circle Color', 'fw'),
			    
			),
		'title'   => array(
			'type'  => 'text',
			'label' => __( 'Title of the Box', 'fw' ),
		 	'value' => 'Icon Title',
		),
		'content' => array(
			'type'  => 'wp-editor',
			'label' => __( 'Content', 'fw' ),
			'value' => 'Contact info ',
			'desc'  => __( 'Enter the Contact info', 'fw' ),
		),

		
		
	 		
		

	
	/*
	'tab2' => array(
		'title' => esc_html__('Extra options', 'fw'),
		'type' => 'tab',
		'options' => array(
	 		
		),
	),
	*/

	
);