<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'general' => array(
	'title' => esc_html__('General', 'fw'),
    'type' => 'tab',
    'options' => array(
		
		'sub-title'  => array(
			'label' => __( 'Sub Title', 'fw' ),
			
			'type'  => 'text',
			'value' => 'Blog'
		),
		'title'   => array(
			'label' => __( 'Title', 'fw' ),
			'desc'  => __( 'Blog Section title', 'fw' ),
			'type'  => 'text',
			'value' => 'Get Every Single Updates.'
		),
        'style' => array(
            'type'  => 'select',
            'value' => 's1',
            
            'label' => __('Style', 'fw'),
            'desc'  => __('Select blog post Style', 'fw'),
            
            'choices' => array(
                
                's1' => __('Style 1', 'fw'),
                's2' => __('Style 2', 'fw'),
               
               
            ),
          
        )

	),
	


	),
	'query_box' => array(
	'title' => esc_html__('Query', 'fw'),
    'type' => 'tab',
    'options' => array(
		 
            'ppp' => array(
                'type' => 'short-text',
                'value' => '3',
                'label' => __('Post Count:', 'fw'),
            ),
            'cat' => array(
                'type'  => 'multi-select',
                'label' => __('Categories:', 'fw'),
                'population' => 'taxonomy',
                'source' => 'category',
                'desc'=> __('Display Posts only from selected category', 'fw'),
            ),
            'isp' => array(
                'type'  => 'checkbox',
                'value' => false, // checked/unchecked
                'label' => __('Ignore Sticky Posts:', 'fw'),
                
                'text'  => __('Yes', 'fw'),
            ),
            'ordr' => array(
                 'type'  => 'select',
                 'value' => 'ASC',
                 'label' => __('Post Order:', 'fw'),
                 'choices' => array(
                    'DESC' => 'DESC',
                    'ASC' => 'ASC',
                 ),
              
	),
	),
	


	),

	
	
);