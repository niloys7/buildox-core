<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg['page_builder'] = array(
	'title'       => __( 'Blog Post', 'fw' ),
	'description' => __( 'Add Blog Posts', 'fw' ),
	'tab'         => __( 'Theme Elements', 'fw' ),
	'popup_size'  => 'medium',
	'icon' => 'fa fa-newspaper-o',
);