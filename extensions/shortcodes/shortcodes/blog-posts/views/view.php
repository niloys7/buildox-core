<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
//fw_print($atts);

// The Query
$id = uniqid('shortcode-');

$args = array(
 			'post_type' => 'post',
 			'order' => $atts['ordr'],  // ASC
 			
 			'ignore_sticky_posts' => $atts['isp'], // true

 			
);
$arg_list = array(
 			'post_type' => 'post',
 			'order' => $atts['ordr'],  // ASC
 			'posts_per_page' => $atts['ppp']+2,
 			'ignore_sticky_posts' => $atts['isp'], // true

 			
);
if($atts['cat']) :
	$args['cat'] = $atts['cat']; //(int) - use category id.'cat' => 5,//(int) - use category id.
endif;
if($atts['style'] == 's2') :
	$args['posts_per_page'] = '2'; 
else :
	$args['posts_per_page'] = $atts['ppp']; 
endif;
//fw_print($atts);
$the_query = new WP_Query( $args );
$the_list = new WP_Query( $arg_list );

// The Loop
if ( $the_query->have_posts() ) {  
?>

<div class="clearfix <?php echo esc_attr( $id  ); ?> blog-section">
	<div class="row justify-content-center">
		<div class="col-lg-6 col-md-8 col-sm-12">
			<div class="section-title mb-30 text-center">
				<span class="sec-subtitle-text mb-30">
				<small class="line-design line-design-left"></small>
					<?php echo esc_html($atts['sub_title']); ?>
				<small class="line-design line-design-right"></small>
				</span>
				<h2 class="sec-title-text"><?php echo esc_html($atts['title']); ?></h2>
			</div>
		</div>
	</div>
	<div class="row">
	<?php
	while ( $the_query->have_posts() ) {
		$the_query->the_post(); ?>
		
		<div class="col-lg-4 col-md-6 col-sm-12">
			<div class="blog-grid-item">

				
				<?php if(has_post_thumbnail()) : ?>
				<div class="blog-image image-container">
					<?php the_post_thumbnail('buildox-portfolio-thumb-400', $attr = '' ); ?>
					<a href="<?php the_permalink();?>" class="plus-effect"></a>
				</div>
				<?php endif; ?>

				<div class="blog-content">
					
					<div class="post-meta ul-li mb-15 clearfix">
						<ul class="clearfix">
							<?php buildox_entry_meta(); ?>
						</ul>
					</div>
					<a href="<?php the_permalink();?>" class="blog-title mb-20"><?php the_title(); ?></a>
					<?php the_excerpt(); ?>
				</div>

			</div>
		</div>
		


	<?php }
	
	// style version 2
	if($atts['style'] == 's2') :
		echo '<div class="col-lg-4 col-md-6 col-sm-12">
			<div class="recent-post ul-li-block clearfix">
				<ul class="clearfix">';
	while ( $the_list->have_posts() ) {
		$the_list->the_post(); ?>
		
		
					<li>
						<div class="post-item">
							<div class="post-meta ul-li mb-15 clearfix">
								<ul class="clearfix">
									<?php buildox_entry_meta(); ?>
								</ul>
							</div>
							<a href="<?php the_permalink();?>" class="item-title">
								<?php the_title(); ?>
							</a>
						</div>
					</li>
					
	<?php }
	echo '</ul></div></div>';
	endif;
	echo '	</div></div>';
	/* Restore original Post Data */
	wp_reset_postdata();
} else {
	// no posts found
	 esc_html_e( 'Sorry, no posts matched your criteria.' ); 
}

?>