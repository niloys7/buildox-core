<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
/**
 * @var array $atts
 */
$id = uniqid('shortcode-');

$image_l = fw_resize( fw_akg('asd/0/img_src/attachment_id', $atts), '310', '350', false ); 
$image_r = fw_resize( fw_akg('asd/1/img_src/attachment_id', $atts), '230', '280', false ); 
$image_b = fw_resize( fw_akg('asd/2/img_src/attachment_id', $atts), '440', '280', false ); 

?>
<div class="about-section">
<div class="grid about-image-masonry zoom-gallery">
							<div class="grid-sizer"></div>

							<div class="grid-item">
								<div class="image-container">
									<img src="<?php echo esc_url($image_l); ?>" alt="buildox-img-box">
								</div>
							</div>
							<div class="grid-item">
								<div class="image-container">
									<img src="<?php echo esc_url($image_r); ?>" alt="buildox-img-box">
								</div>
							</div>
							<div class="grid-item grid-item-width">
								<div class="image-container">
									<img src="<?php echo esc_url($image_b); ?>" alt="buildox-img-box">
									<a href="#!" class="home-btn">
										<span class="logo-img">
											<img src="<?php echo esc_url($atts['img_logo']['url']); ?>" alt="logo">
										</span>
									</a>
								</div>
							</div>
						</div>
						</div>