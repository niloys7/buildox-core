<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
//fw_print(fw_ext('shortcodes'));
$options = array(

	'asd' =>	array(
    'type' => 'addable-popup',
    
    'label' => __('Image', 'fw'),
    
    'template' => '{{- img_tile }}',
    'popup-title' => null,
    'size' => 'small', // small, medium, large
    'limit' => 3, // limit the number of popup`s that can be added
    'add-button-text' => __('Add', 'fw'),
    'sortable' => false,
    'popup-options' => array(
    	'img_tile' => array(
    		'type' => 'text',
    		'label' => __('Image Title', 'fw'),
    	),
        'img_src' => array(
				'type'          => 'upload',
				'label' => __('Image', 'fw'),
				'images_only' => true,
				
			)
       
    ),
),
	'img_logo' => array(
				'type'          => 'upload',
				'label' => __('Logo', 'fw'),
				'images_only' => true,
				
			)
							
						

	
	/*
	'tab2' => array(
		'title' => esc_html__('Extra options', 'fw'),
		'type' => 'tab',
		'options' => array(
	 		
		),
	),


	'button' => array(
				'type'          => 'popup',
				'popup-title'   => __( 'Add New Image', 'fw' ),
				'button'        => __( 'Add', 'fw' ),
				'popup-options' => fw_ext('shortcodes')->get_shortcode('media_image')->get_options()
			)


	*/

	
);