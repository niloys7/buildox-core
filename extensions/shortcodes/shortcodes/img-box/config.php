<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => __('Image Box', 'fw'),
	'description'   => __('Add an Image Box', 'fw'),
	'tab'           => __('Theme Elements', 'fw'),
	'icon' => 'fa fa-photo',
);