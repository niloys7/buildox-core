<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg['page_builder'] = array(
	'title'       => __( 'Table', 'fw' ),
	'description' => __( 'Add a Table', 'fw' ),
	'tab'         => __( 'Theme Elements', 'fw' ),
	'popup_size'  => 'large',
	'icon' => 'fa fa-table',
);