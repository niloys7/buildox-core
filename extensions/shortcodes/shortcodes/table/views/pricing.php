<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

/**
 * @var array $atts
 */

$class_width = 'fw-col-md-' . ceil( 12 / count( $atts['table']['cols'] ) );

/** @var FW_Extension_Shortcodes $shortcodes */
$shortcodes = fw_ext( 'shortcodes' );
/** @var FW_Shortcode_Table $table */
$table = $shortcodes->get_shortcode( 'table' );

?>
<div class="pricing-section">
	<?php foreach ( $atts['table']['cols'] as $col_key => $col ): 
		$id = uniqid('table-');
		?>
		
        <div class="<?php echo esc_attr( $id. ' ' .$class_width); ?> ">
        <div class="pricing-plan-item text-center">
            
				<?php foreach ( $atts['table']['rows'] as $row_key => $row ): ?>
					<?php if ( $col['name'] == 'desc-col' ) : ?>
                        <div class="fw-default-row">
							<?php echo fw_akg( 'textarea', $atts['table']['content'][ $row_key ][ $col_key ], '' ); ?>
                        </div>
						<?php continue; endif; ?>
					<?php if ( $row['name'] === 'heading-row' ): ?>
                       
                    	<h3 class="item-title mb-15">
                           
								<?php echo fw_akg(
									'textarea',
									$atts['table']['content'][ $row_key ][ $col_key ],
									$col['name'] === 'desc-col' ? '&nbps;' : ''
								); ?>
							
                        </h3>
					<?php elseif ( $row['name'] === 'pricing-row' ): ?>
                        
                        <h4 class="amount-text mb-30">
                          
                             <?php echo '<sub>'.fw_akg(
									'description',
									$atts['table']['content'][ $row_key ][ $col_key ],
									$col['name'] === 'desc-col' ? '&nbps;' : ''
								) .'</sub>';?>
								<?php echo fw_akg(
		                            'amount',
		                            $atts['table']['content'][ $row_key ][ $col_key ],
		                            $col['name'] === 'desc-col' ? '&nbps;' : ''
	                            ) ?>
	                            
                        </h4>
					
					<?php elseif ( $row['name'] == 'icon-row' ) : ?>
						<?php if ( $icon = fw_ext('shortcodes')->get_shortcode('table_icon') ): ?>
							
                           
								<?php if ( false === empty( $atts['table']['content'][ $row_key ][ $col_key ]['icon'] ) and false === empty( $icon ) ) : ?>
									<?php echo $icon->render( $atts['table']['content'][ $row_key ][ $col_key ]['icon'] ); ?>
								<?php else : ?>
                                    <span>&nbsp;</span>
								<?php endif; ?>
                            
					<?php endif; ?>
					<?php elseif ( $row['name'] == 'button-row' ) : ?>
						<?php if ( $button = fw_ext('shortcodes')->get_shortcode('table_btn') ): ?>
							
                           
								<?php if ( false === empty( $atts['table']['content'][ $row_key ][ $col_key ]['button'] ) and false === empty( $button ) ) : ?>
									<?php echo $button->render( $atts['table']['content'][ $row_key ][ $col_key ]['button'] ); ?>
								<?php else : ?>
                                    <span>&nbsp;</span>
								<?php endif; ?>
                            
					<?php endif; ?>
					<?php elseif ( $row['name'] === 'switch-row' ) : ?>
                        <div class="fw-switch-row">
							<?php $value = $atts['table']['content'][ $row_key ][ $col_key ]['switch']; ?>
                            <span>
								<i class="fa fw-price-icon-<?php echo esc_attr( $value ) ?>"></i>
							</span>
                        </div>
                        
                   
                        	<?php elseif ( $row['name'] === 'default-row' ) : ?>
                        	<div class="fw-table-row"><?php
							echo fw_akg( "textarea", $atts['table']['content'][ $row_key ][ $col_key ] )
							?></div>
							<?php endif; ?>
                      
                        
					
				<?php endforeach; ?>
            
        </div>
        </div>
	<?php endforeach; ?>
</div>