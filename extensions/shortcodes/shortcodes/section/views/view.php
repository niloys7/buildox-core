<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
$id = uniqid('section-');
$bg_color = '';
if ( ! empty( $atts['background_color'] ) ) {
	$bg_color = 'background-color:' . $atts['background_color'] . ';';
}

$bg_image = '';
if ( ! empty( $atts['background_image'] ) && ! empty( $atts['background_image']['data']['icon'] ) ) {
	$bg_image = 'background-image:url(' . $atts['background_image']['data']['icon'] . ');';
}

$bg_video_data_attr    = '';
$section_extra_classes = '';
if ( ! empty( $atts['video'] ) ) {
	$filetype           = wp_check_filetype( $atts['video'] );
	$filetypes          = array( 'mp4' => 'mp4', 'ogv' => 'ogg', 'webm' => 'webm', 'jpg' => 'poster' );
	$filetype           = array_key_exists( (string) $filetype['ext'], $filetypes ) ? $filetypes[ $filetype['ext'] ] : 'video';
	$data_name_attr = version_compare( fw_ext('shortcodes')->manifest->get_version(), '1.3.9', '>=' ) ? 'data-background-options' : 'data-wallpaper-options';
	$bg_video_data_attr = $data_name_attr.'="' . fw_htmlspecialchars( json_encode( array( 'source' => array( $filetype => $atts['video'] ) ) ) ) . '"';
	$section_extra_classes .= ' background-video';
}
$defaut_padding = ($atts['pad_check']['enabled'] == 'false') ? ' sec-ptb-140 clearfix ' : '';

$padding_top = ( $atts['pad_check']['true']['pd']['top']) ? 'padding-top: '. esc_attr($atts['pad_check']['true']['pd']['top']) .';' : '';
$padding_bottom = ( $atts['pad_check']['true']['pd']['bottom']) ? 'padding-bottom: '. esc_attr($atts['pad_check']['true']['pd']['bottom']) .';' : '';
$padding_right = ( $atts['pad_check']['true']['pd']['right']) ? 'padding-right: '. esc_attr($atts['pad_check']['true']['pd']['right']) .';' : '';
$padding_left = ( $atts['pad_check']['true']['pd']['left']) ? 'padding-left: '. esc_attr($atts['pad_check']['true']['pd']['left']) .';' : '';



$section_style   =  ( $bg_color || $bg_image ) ? 'style="'.$padding_top.''.$padding_bottom.''.$padding_right.''.$padding_left.'"' : 'style="'.$padding_top.''.$padding_bottom.''.$padding_right.''.$padding_left.'"' ;

$container_class = ( isset( $atts['is_fullwidth'] ) && $atts['is_fullwidth'] ) ? 'fw-container-fluid' : 'fw-container';
?>
<div class="fw-main-row  <?php echo esc_attr($id.' '.$section_extra_classes .' '.$atts['css_class'] ) ?>" <?php echo $bg_video_data_attr; ?>>
<div <?php echo $section_style; ?> data-image-src="<?php echo esc_url($atts['background_image']['data']['icon']); ?>" class="<?php echo $defaut_padding; ?> parallax-window"  data-parallax="scroll">
<div class="overlay" style="<?php echo esc_attr($bg_color) ?>"></div>
	
	
	<div  class="<?php echo esc_attr($container_class); ?>">
		<?php echo do_shortcode( $content ); ?>
	</div>

</div>
</div>
