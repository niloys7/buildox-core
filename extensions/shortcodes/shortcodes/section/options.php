<?php if (!defined('FW')) {
	die('Forbidden');
}

$options = array(
	'is_fullwidth' => array(
		'label'        => __('Full Width', 'fw'),
		'type'         => 'switch',
	),
	'pad_check' => array(
		'type' => 'multi-picker',
		'label' => false,
		'desc' => false,
		'picker' => array(
				'enabled' => array(
						'label' => esc_html__('Section Padding', 'fw'),
						'type' => 'switch',
						'left-choice' => array(
								'value' => 'false',
								'label' => esc_html__('Default', 'fw')
						),
						'right-choice' => array(
								'value' => 'true',
								'label' => esc_html__('Custom', 'fw')
						),
						'value' => 'false',
						'desc' => 'Default : 140px in Top & Bottom'

				)
		),
		'choices' => array(
				'true' => array(
					'pd' => array(
						'type' => 'stylebox',
						'label'        => __('Padding', 'fw'),
					),
				)
		)
						),
	
	'css_id' => array(
		'label' => __('CSS ID', 'fw'),
		'desc'  => __('Custom CSS ID', 'fw'),
		'type'  => 'text',
	),

	'css_class' => array(
		'label' => __('CSS Class', 'fw'),
		'desc'  => __('Custom CSS Class', 'fw'),
		'type'  => 'text',
	),
	
	'background_color'		 => array(
		'type'		 => 'rgba-color-picker',
		'value'		 => '#fff',
		// palette colors array
		'label'		 => esc_html__( 'Background Color', 'fw' ),
		'desc'  => __('Please select the background color', 'fw'),
	),
	'background_image' => array(
		'label'   => __('Background Image', 'fw'),
		'desc'    => __('Please select the background image', 'fw'),
		'type'    => 'background-image',
		'choices' => array(//	in future may will set predefined images
		)
	),
	'video' => array(
		'label' => __('Background Video', 'fw'),
		'desc'  => __('Insert Video URL to embed this video', 'fw'),
		'type'  => 'text',
	)
);
