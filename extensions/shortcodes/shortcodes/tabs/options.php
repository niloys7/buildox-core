<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'id'    => array( 'type' => 'unique' ),
	'tabs' => array(
		'type'          => 'addable-popup',
		'label'         => __( 'Tabs', 'fw' ),
		'popup-title'   => __( 'Add/Edit Tab', 'fw' ),
		'desc'          => __( 'Create your tabs', 'fw' ),
		'template'      => '{{=tab_title}}',
		'size' => 'medium', // small, medium, large
		'popup-options' => array(
			'tab_title' => array(
				'type'  => 'text',
				'label' => __('Title', 'fw')
			),
			'tab_icon' => array(
				'type'  => 'icon-v2',
				'label' => __('Tab Icon', 'fw')
			),
			'tab_icon_color' =>  array(
			    'type'  => 'color-picker',
			    'value' => '#ff4d1c',
			    
			    // palette colors array
			    'palettes' => array( '#f7c605', '#ff4d1c','#009326','#006cff','#9227ff','#0cff84'),
			    'label' => __('Icon Color', 'fw'),
			    
			),
			'tab_content' => array(
				'type'  => 'wp-editor',
				'label' => __('Content', 'fw'),
				
    			'wpautop' => false,
    			'shortcodes' => true // true,
			)
		),
	)
);