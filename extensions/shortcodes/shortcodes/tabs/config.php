<?php if (!defined('FW')) {
	die('Forbidden');
}

$cfg = array();

$cfg['page_builder'] = array(
	'disable_correction' => true,
	'title' => __('Tabs', 'fw'),
	'description' => __('Add some Tabs', 'fw'),
	'tab' => __('Theme Elements', 'fw'),
	'icon' => 'fa fa-list-alt',
);