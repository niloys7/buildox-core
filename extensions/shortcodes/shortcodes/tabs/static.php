<?php if (!defined('FW')) die('Forbidden');

$shortcodes_extension = fw_ext('shortcodes');


wp_enqueue_script(
	'fw-shortcode-tabsq',
	$shortcodes_extension->get_uri('/shortcodes/tabs/static/js/scripts.js'),
	array('jquery-ui-tabs'),
	false,
	true
);
wp_dequeue_script( 'fw-shortcode-tabs' );