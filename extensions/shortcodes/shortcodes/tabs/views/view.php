<?php if (!defined('FW')) die( 'Forbidden' ); ?>
<?php 
$tabs_id = uniqid('fw-tabs-'); 

//fw_print($atts);
?>


<div class="fw-tabs-container portfolio-section <?php echo esc_attr(' shortcode-' . $atts['id']); ?>" id="<?php echo esc_attr($tabs_id); ?>">
	<div class=" portfolio-tab-nav">
		<ul class="nav">
			<?php foreach (fw_akg( 'tabs', $atts, array() ) as $key => $tab) : ?>
				<li class="nav-item"><a class="nav-link" href="#<?php echo esc_attr($tabs_id . '-' . ($key + 1)); ?>">
					<span class="icon color-default-yellow"><i style="color:<?php echo esc_attr($tab['tab_icon_color'] ); ?>;" class="<?php echo esc_attr($tab['tab_icon']['icon-class'] ); ?>"></i></span>
					<strong class="title-text"><?php echo $tab['tab_title']; ?></strong>
					
				</a></li>
			<?php endforeach; ?>
			
		</ul>
	</div>
	<?php foreach ( $atts['tabs'] as $key => $tab ) : ?>
		<div class="row" id="<?php echo esc_attr($tabs_id . '-' . ($key + 1)); ?>">
			<?php echo do_shortcode( $tab['tab_content'] ) ?>
			
		</div>
	<?php endforeach; ?>
</div>