<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'tem_d' => array(
	'title' => esc_html__('Details', 'fw'),
    'type' => 'tab',
    'options' => array(
		'image' => array(
			'label' => __( 'Team Member Image', 'fw' ),
			'desc'  => __( 'Either upload a new, or choose an existing image from your media library', 'fw' ),
			'type'  => 'upload'
		),
		'name'  => array(
			'label' => __( 'Team Member Name', 'fw' ),
			'desc'  => __( 'Name of the person', 'fw' ),
			'type'  => 'text',
			'value' => ''
		),
		'job'   => array(
			'label' => __( 'Team Member Job Title', 'fw' ),
			'desc'  => __( 'Job title of the person.', 'fw' ),
			'type'  => 'text',
			'value' => ''
		),
	),
	


	),
	'tem_s' => array(
	'title' => esc_html__('Social Profiles', 'fw'),
    'type' => 'tab',
    'options' => array(
		 'fb' => array(
                'type' => 'text',
                'label' => __('Facebook:', 'fw'),
            ),
            'tw' => array(
                'type' => 'text',
                'label' => __('Twitter:', 'fw'),
            ),
            'ig' => array(
                'type' => 'text',
                'label' => __('Instagram:', 'fw'),
            ),
            'yt' => array(
                'type' => 'text',
                'label' => __('Youtube:', 'fw'),
            ),
            'ld' => array(
                'type' => 'text',
                'label' => __('Linkedin:', 'fw'),
            ),
	),
	


	),

	
	
);