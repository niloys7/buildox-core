<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg['page_builder'] = array(
	'title'       => __( 'Team', 'fw' ),
	'description' => __( 'Add a Team Member', 'fw' ),
	'tab'         => __( 'Theme Elements', 'fw' ),
	'popup_size'  => 'medium',
	'icon' => 'fa fa-user',
	 'title_template' => '{{- title }} <span class="cix-title-template"> Image ID: {{= o.image.attachment_id }} <br>Name: {{= o.name }} <br>Postion: {{= o.job }} <br></span>',
);