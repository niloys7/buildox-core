<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
//fw_print($atts);
if ( empty( $atts['image'] ) ) {
	$image = fw_get_framework_directory_uri('/static/img/no-image.png');
} else {
	$image = $atts['image']['url'];
}
?>
<div class="team-section">
<div class="team-grid-item text-center clearfix">
	<div class="image-container">
		<img src="<?php echo esc_url($image); ?>" alt="Team Member">
	</div>
	<div class="member-info">
		<h2 class="member-name"><?php echo esc_html($atts['name']); ?></h2>
		<span class="member-title mb-15"><?php echo esc_html($atts['job']); ?></span>
		<?php if($atts['fb'] || $atts['tw'] || $atts['pr'] || $atts['yt'] || $atts['ld']): ?>
		<div class="social-links ul-li-center clearfix">
			<ul class="clearfix">
				<?php if($atts['fb']): ?>
					<li><a href="<?php echo esc_url($atts['fb']); ?>"><i class="fab fa-facebook-f"></i></a></li>
				<?php endif; ?>

				<?php if($atts['tw']): ?>
					<li><a href="<?php echo esc_url($atts['tw']); ?>"><i class="fab fa-twitter"></i></a></li>
					
				<?php endif; ?>

				<?php if($atts['yt']): ?>	
				<li><a href="<?php echo esc_url($atts['yt']); ?>"><i class="fab fa-youtube"></i></a></li>
				<?php endif; ?>

				<?php if($atts['ig']): ?>
					
				<li><a href="<?php echo esc_url($atts['ig']); ?>"><i class="fab fa-instagram"></i></a></li>
				<?php endif; ?>

				<?php if($atts['ld']): ?>
					
				<li><a href="<?php echo esc_url($atts['ld']); ?>"><i class="fab fa-linkedin"></i></a></li>
				<?php endif; ?>

				
				
			</ul>
		</div>
		<?php endif; ?>
	</div>
</div>
</div>