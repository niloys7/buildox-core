<?php if (!defined('FW')) die('Forbidden');

$css_classes = $atttibutes = array();

$class = fw_ext_builder_get_item_width('page-builder', $atts['width'] . '/frontend_class');

/**
 * Animations
 **/
if( isset( $atts['animation']['enabled'] ) && filter_var( $atts['animation']['enabled'], FILTER_VALIDATE_BOOLEAN ) ) {
	$css_classes[] = 'wow';
	$css_classes[] = $atts['animation']['true']['effect']['0'];
	$atttibutes[] = 'data-wow-delay="' . esc_attr( $atts['animation']['true']['animation_delay'] ) . '"';
}
//fw_print($css_classes ,$atts);
?>
<div class="<?php echo esc_attr( $class ); ?> <?php echo implode( ' ', $css_classes ); ?>" <?php echo implode( ' ', $atttibutes ); ?>>
	<?php echo do_shortcode( $content ); ?>
</div>