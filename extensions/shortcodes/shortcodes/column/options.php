<?php if (!defined('FW')) die('Forbidden');
$options = array (
    'styling' => array(
			'title' => esc_html__( 'Styling', 'fw' ),
			'type' => 'box',
			'options' => array(
				
				'animation' => array(
					'type' => 'multi-picker',
					'label' => false,
					'desc' => false,
					'picker' => array(
						'enabled' => array(
							'label' => esc_html__( 'Animate this column?', 'fw' ),
							'desc' => esc_html__('Using CSS animation', 'fw'),
							'type' => 'switch',
							'right-choice' => array(
								'value' => 'true',
								'label' => esc_html__( 'Yes', 'fw' )
							),
							'left-choice' => array(
								'value' => 'false',
								'color' => '#ccc',
								'label' => esc_html__( 'No', 'fw' )
							),
							'value' => 'false',
						)
					),
					'choices' => array(
						'true' => array(

							'effect' => array(
								'type'  => 'multi-select',
								'label' => esc_html__('Choose animation effect', 'fw'),
								'desc'  => esc_html__('Animate.css Library', 'fw'),
								'population' => 'array',
								'source' => '',
								'choices' => buildox_animate_css(),
								'limit' => 1,
							),

							'animation_delay' => array(
								'label' => esc_html__('Animation delay', 'fw'),
								'desc' => esc_html__('For example: 0.3s', 'fw'),
								'type' => 'text',
							),

						),
					),
					'show_borders' => false,
				),
			
			
			)
        )
            );