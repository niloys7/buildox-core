<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$id = uniqid('shortcode-');
//fw_print($atts);
// fw_print($id);


// fw_print(buildox_hex2rgba($atts['icon_color'],'0.1'));
$i = 0;
?>
<div class="service-section">
<div class="service-item-list ul-li clearfix">
	<ul class="clearfix">
		<?php foreach ($atts['tabs'] as $icon) { 
$i++;

?>
		<li class="<?php echo esc_attr('shortcode-' . $atts['id'].$i); ?>">
			<div class="service-item-rounded text-center bg-default-yellow">
				<span class="icon"><i class="<?php echo $icon['icon']['icon-class']; ?>"></i></span>
				<a href="<?php echo $icon['link'];?>" class="service-title mb-15"><?php echo $icon['title']; ?></a>
				<p class="mb-0">
					<?php echo $icon['content']; ?>
				</p>
			</div>
		</li>
	
		
		<?php } ?>
	
	</ul>
</div>

</div>