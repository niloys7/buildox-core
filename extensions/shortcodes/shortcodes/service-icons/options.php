<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'id'    => array( 'type' => 'unique' ),
	'tabs' => array(
		'type'          => 'addable-popup',
		'label'         => __( 'Icons', 'fw' ),
		'popup-title'   => __( 'Add/Edit Icons', 'fw' ),
		'desc'          => __( 'Create Icons', 'fw' ),
		'template'      => '{{=title}}',
		'popup-options' => fw_ext('shortcodes')->get_shortcode('icon_box_service')->get_options()
	),
	

				
);
