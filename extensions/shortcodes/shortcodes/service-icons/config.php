<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg['page_builder'] = array(
	'disable_correction' => true,
	'title'       => __( 'Service Icons', 'fw' ),
	'description' => __( 'Add service icons', 'fw' ),
	'tab'         => __( 'Theme Elements', 'fw' ),
);