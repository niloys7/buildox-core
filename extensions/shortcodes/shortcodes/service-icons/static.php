<?php if (!defined('FW')) die('Forbidden');



if (!function_exists('_action_theme_shortcode_service_icons_enqueue_dynamic_css')):

    /**
     * @internal
     * @param array $data
     */
    function _action_theme_shortcode_service_icons_enqueue_dynamic_css($data) {
        $shortcode = 'service_icons';
        $atts = shortcode_parse_atts( $data['atts_string'] );
        $atts = fw_ext_shortcodes_decode_attr($atts, $shortcode, $data['post']->ID);
       

       
        $i = 0;
    foreach ($atts['tabs'] as $icon) { 
        $i++;
        $hexColor = buildox_hex2rgba($icon['icon_color'],'0.1');
        wp_add_inline_style(
            'buildox-core',
            '.shortcode-'.$atts['id'].$i.' .service-item-rounded .icon { 
                '.'background-color: '. $hexColor.';'.' 
            } 
            .shortcode-'.$atts['id'].$i.':hover .service-item-rounded{ 
                '.'background-color: '. $icon['icon_color'] .';'.' 
            } 
            .shortcode-'.$atts['id'].$i.' .service-item-rounded .icon i{ 
                '.'color: '. $icon['icon_color'] .';'.' 
            } 
            .shortcode-'.$atts['id'].$i.':hover .service-item-rounded{ 
                
                '.'box-shadow: 0px 5px 30px 1px '. buildox_hex2rgba($icon['icon_color'],'0.8') .';
                
            } 
          
            
            '
            
            
            
        );
    }
    }
    add_action(
        'fw_ext_shortcodes_enqueue_static:service_icons',
        '_action_theme_shortcode_service_icons_enqueue_dynamic_css'
    );
    
    endif;