<?php if (!defined('FW')) {
	die('Forbidden');
}

$options = array(
	'boxs' => array(
		'type' => 'addable-popup',
		'label' => __('Feature Box', 'fw'),
		'popup-title' => __('Add/Edit Feature Box', 'fw'),
		'desc' => __('Create your Feature Boxs', 'fw'),
		'template' => '{{=box_title}}',
		'popup-options' => array(
			'box_icon' => array(
				'type' => 'icon-v2',
				'label' => __('Box Icon', 'fw'),
			),
			'box_i_color' => array(
				'type' => 'color-picker',
				'label' => __('Box Icon Color', 'fw'),

				'value' => '#ff4d1c',

				// palette colors array
				'palettes' => array('#f7c605', '#ff4d1c', '#009326', '#006cff', '#9227ff', '#0cff84'),

			),
			'box_title' => array(
				'type' => 'text',
				'label' => __('Title', 'fw'),
			),
			'box_content' => array(
				'type' => 'textarea',
				'label' => __('Content', 'fw'),
			),
			'box_link' => array(
				'type' => 'text',
				'label' => __('Button Link', 'fw'),
			),
			'box_btn_txt' => array(
				'type' => 'textarea',
				'label' => __('Button Text', 'fw'),
			),
			'box_img' => array(
				'type' => 'upload',
				'label' => __('Box Hover Image', 'fw'),
			),
		),
	),
);