<?php if (!defined('FW')) {
	die('Forbidden');
}

$cfg = array();

$cfg['page_builder'] = array(
	'title' => __('Feature Box', 'fw'),
	'description' => __('Add Feature Box', 'fw'),
	'tab' => __('Theme Elements', 'fw'),
);