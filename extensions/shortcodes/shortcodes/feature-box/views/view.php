<?php if (!defined('FW')) {
	die('Forbidden');
}
//fw_print($atts);
$i = 1;
?>
<div class="feature-section">
	<div class="feature-carousel owl-carousel owl-theme">
			<?php foreach (fw_akg('boxs', $atts, array()) as $box):

	$img_src = wp_get_attachment_image_src($box['box_img']['attachment_id'], $size = 'buildox-portfolio-thumb');
	?>
		<div class="item" style="background-image: url(<?php echo $img_src[0]; ?>);">
			<div class="feature-item">

				<small class="serial-number"><?php echo '0' . $i++; ?></small>
					<span style="color:<?php echo $box['box_i_color']; ?>" class="icon mb-30"><i class="<?php echo $box['box_icon']['icon-class']; ?>"></i></span>
					<h3 class="feature-title mb-15">
						<?php echo $box['box_title']; ?>

					</h3>
					<p class="mb-20">
						<?php echo $box['box_content']; ?>
					</p>
					<a href="<?php echo $box['box_link']; ?>" class="details-btn"><?php echo $box['box_btn_txt']; ?></a>
				</div>
		</div>


													<?php endforeach;?>


	</div>
</div>