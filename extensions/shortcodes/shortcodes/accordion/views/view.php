<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
$i = 1;
?>
<div class="fw-accordion">
	<?php foreach ( fw_akg( 'tabs', $atts, array() ) as $tab ) : ?>
		<div class="fw-accordion-title"><span><?php echo '0'.$i++; ?></span><?php echo $tab['tab_title']; ?></div>
		<div class="fw-accordion-content">
			<p><?php echo do_shortcode( $tab['tab_content'] ); ?></p>
		</div>
	<?php endforeach; ?>
</div>