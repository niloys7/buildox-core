<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg['page_builder'] = array(
	'title'       => __( 'Accordion', 'fw' ),
	'description' => __( 'Add an Accordion', 'fw' ),
	'tab'         => __( 'Theme Elements', 'fw' ),
);