<?php if (!defined('FW')) die( 'Forbidden' ); 
$id = uniqid('table-icon-');
?>

<span class="icon mb-15 <?php echo esc_attr($id); ?>"><i style="color:<?php echo esc_attr($atts['color']); ?>" class="<?php echo esc_attr($atts['icon']['icon-class']); ?>"></i></span>

