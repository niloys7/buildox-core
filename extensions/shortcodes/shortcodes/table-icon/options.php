<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'icon'  => array(
		'label' => __( 'Icon', 'fw' ),
		'desc'  => __( 'Select an Icon for Table', 'fw' ),
		'type'  => 'icon-v2',
		
	),
	'color' => array(
			    'type'  => 'color-picker',
			    'value' => '#ff4d1c',
			    
			    // palette colors array
			    'palettes' => array( '#f7c605', '#ff4d1c','#009326','#006cff','#9227ff','#0cff84'),
			    'label' => __('Icon Color', 'fw'),
			    
			),
	
);