<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg['page_builder'] = array(
	'title'       => __( 'Call To Action', 'fw' ),
	'description' => __( 'Add a Call to Action', 'fw' ),
	'tab'         => __( 'Theme Elements', 'fw' ),
	'icon'		=> 'fa fa-info',
	'title_template' => '{{- title }} <span class="cix-title-template"> Text : {{= o.message }} </span>',
);