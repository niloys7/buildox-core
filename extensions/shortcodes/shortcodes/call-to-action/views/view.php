<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
} ?>
<div class="sec-ptb-140 calltoaction-section">
	<div class="container">
		<div class="row align-items-center justify-content-lg-start justify-content-md-center">

			<div class="col-lg-7 col-md-10 col-sm-12">
				<div class="section-title">
					<?php if (!empty($atts['title'])): ?>
					<span class="sec-subtitle-text mb-30"><?php echo $atts['title']; ?></span>
					<?php endif; ?>
					
					<h2 class="sec-title-text"><?php echo $atts['message']; ?></h2>
				</div>
			</div>
			<div class="col-lg-5 col-md-6 col-sm-12">
				<div class="quote-btn text-right">
					<a href="<?php echo esc_attr($atts['button_link']); ?>" class="custom-btn" target="<?php echo esc_attr($atts['button_target']); ?>">
						<span class="btn-bg bg-default-yellow"><?php echo $atts['button_label']; ?></span>
					</a>
					
				</div>
			</div>

		</div>
	</div>
</div>