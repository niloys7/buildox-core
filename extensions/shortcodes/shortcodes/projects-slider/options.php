<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(

	'query_box' => array(
	'title' => esc_html__('Project Query', 'fw'),
    'type' => 'box',
    'options' => array(
		 
            'ppp' => array(
                'type' => 'short-text',
                'value' => '4',
                'label' => __('Post Count:', 'fw'),
            ),
            'cat' => array(
                'type'  => 'multi-select',
                'label' => __('Categories:', 'fw'),
                'population' => 'taxonomy',
                'source' => 'fw-portfolio-category',
                'desc'=> __('Display Posts only from selected category', 'fw'),
            ),
            'ordr' => array(
                 'type'  => 'select',
                 'value' => 'DESC',
                 'label' => __('Post Order:', 'fw'),
                 'choices' => array(
                    'DESC' => 'DESC',
                    'ASC' => 'ASC',
                 ),
              
	),
	),
	


	),

	
	
);