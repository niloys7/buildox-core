<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
//fw_print($atts);
$id = uniqid('slider-');
// The Query
$args = array(
 			'post_type' => 'fw-portfolio',
 			'order' => $atts['ordr'],  // ASC
 			'posts_per_page' => $atts['ppp'], 
 			'cat' => $atts['cat'],//(int) - use category id.'cat' => 5,//(int) - use category id.
);
$the_query = new WP_Query( $args );

// The Loop
if ( $the_query->have_posts() ) {  
	
?>
<div class="portfolio-section">
<div id="<?php echo $id;?>" class="portfolio-carousel owl-carousel owl-theme">
		<?php
	while ( $the_query->have_posts() ) {
		$the_query->the_post(); ?>

	<div class="item">
		<a href="<?php the_permalink(); ?>" class="details-link">
			<?php the_post_thumbnail( $size = 'buildox-portfolio-thumb', $attr = '' );?>
		</a>
		<ul class="action-btns-group clearfix">
			<!-- <li><a href="<?php // the_permalink(); ?>"><i class="flaticon-magnifying-glass"></i></a></li> -->
			<li><a href="<?php the_permalink(); ?>"><i class="fa fa-link"></i></a></li>
		</ul>
	</div>
				


	<?php } ?>
			 

	
</div>
</div>
<script>
jQuery( document ).ready(function() {
   	  // portfolio-carousel - start
  // --------------------------------------------------
  jQuery('#<?php echo $id;?>').owlCarousel({
    nav:true,
    loop:false,
    margin:30,
    dots:false,
    responsive:{
      0:{
        items:1
      },
      600:{
        items:2
      },
      1000:{
        items:3
      },
      1920:{
        items:3
      }
    }
  })
  // portfolio-carousel - end
  // --------------------------------------------------
});


</script>
<?php
} else {
	// no posts found
	 esc_html_e( 'Sorry, no posts matched your criteria.' ); 
}

?>
