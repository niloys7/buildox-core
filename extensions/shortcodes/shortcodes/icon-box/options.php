<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'id'    => array( 'type' => 'unique' ),
	'tab1' => array(
		'title' => esc_html__('General', 'fw'),
		'type' => 'tab',
		'options' => array(
			
		'icon'    => array(
			'type'  => 'icon-v2',
			'label' => __('Choose an Icon', 'fw'),
		),
		'title'   => array(
			'type'  => 'text',
			'label' => __( 'Title of the Box', 'fw' ),
		 	'value' => 'Icon Title',
		),
		'content' => array(
			'type'  => 'textarea',
			'label' => __( 'Content', 'fw' ),
			'value' => 'Icon desired content : Lorem ipsum dolor sit amet, consectetu r adipisicing elit, sed do eiusmod temp incididunt ut labore et dolore. ',
			'desc'  => __( 'Enter the desired content', 'fw' ),
		),

		'link_txt'   => array(
			'type'  => 'text',
			'label' => __( 'Button Text', 'fw' ),
		 	
		),
		'link_url'   => array(
			'type'  => 'text',
			'label' => __( 'Button Link', 'fw' ),
		 	'value' => '#',
		),
	 		
		),
	),

	'tab2' => array(
		'title' => esc_html__('Style', 'fw'),
		'type' => 'tab',
		'options' => array(
			'style'   => array(
		    'type'  => 'image-picker',
		    'value' => 'value-1',
		    
		    'label' => __('Box Style', 'fw'),
		    
		    'choices' => array(
		        'value-1' => array(
		        	'small' => array(
		                'src' => BUILDOX_IMAGES .'/wp/icon_1.png',
		                'height' => 100
		            ),
		        	'large' => array(
		                'src' => BUILDOX_IMAGES .'/wp/icon_1.png',
		                'height' => 250
		            ),
	        	),
		        'value-2' => array(
		        	'small' => array(
		                'src' => BUILDOX_IMAGES .'/wp/icon_2.png',
		                'height' => 100
		            ),
		        	'large' => array(
		                'src' => BUILDOX_IMAGES .'/wp/icon_2.png',
		                'height' => 250
		            ),
	        	),
		        'value-3' => array(
		        	'small' => array(
		                'src' => BUILDOX_IMAGES .'/wp/icon_3.png',
		                'height' => 100
		            ),
		        	'large' => array(
		                'src' => BUILDOX_IMAGES .'/wp/icon_3.png',
		                'height' => 200
		            ),
	        	),
		        
		       
		    ),
		    'blank' => false, // (optional) if true, images can be deselected
		),
			'icon_color' => array(
			    'type'  => 'color-picker',
			    'value' => '#ff4d1c',
			    
			    // palette colors array
			    'palettes' => array( '#f7c605', '#ff4d1c','#009326','#006cff','#9227ff','#0cff84'),
			    'label' => __('Icon Color', 'fw'),
			    
			),
			'anchor_c' => array(
			    'type'  => 'color-picker',
			    'label' => __('Anchor Hover Color', 'fw'),
			    'value' => '#ff4d1c',
			    'palettes' => array(),
			   
			    
			),
	 		
		),
	),
	/*
	'tab2' => array(
		'title' => esc_html__('Extra options', 'fw'),
		'type' => 'tab',
		'options' => array(
	 		
		),
	),
	*/

	
);
