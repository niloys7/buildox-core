<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => __('Icon Box', 'fw'),
	'description'   => __('Add an Icon Box', 'fw'),
	'tab'           => __('Theme Elements', 'fw'),
	'icon' => 'flaticon flaticon-star',
	 'title_template' => '{{- title }} <span class="cix-title-template"> Title: {{= o.title }}</span>',
);
?>