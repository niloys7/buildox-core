<?php if (!defined('FW')) die('Forbidden');


wp_dequeue_script('fw-shortcode-icon-box');


if (!function_exists('_action_theme_shortcode_icon_box_enqueue_dynamic_css')):

    /**
     * @internal
     * @param array $data
     */
    function _action_theme_shortcode_icon_box_enqueue_dynamic_css($data) {
        $shortcode = 'icon_box';
        $atts = shortcode_parse_atts( $data['atts_string'] );
        $atts = fw_ext_shortcodes_decode_attr($atts, $shortcode, $data['post']->ID);

        wp_add_inline_style(
            'buildox-core',
            '.shortcode-'. $atts['id'] .' .service-bordered-item .details-btn:hover,
            .shortcode-'. $atts['id'] .' .feature-content .details-btn:hover,
            .shortcode-'. $atts['id'].' .service-section a{ '.
                'color: '. $atts['anchor_c'] .';'.
            ' } 
            
            .shortcode-'. $atts['id'] .' .service-bordered-item .details-btn:hover::after { '.
                'color: '. $atts['anchor_c'] .';'.
                
            ' }
            .shortcode-'. $atts['id'] .' .service-grid-item:hover .details-btn { '.
                'border-color: '. $atts['anchor_c'] .';'.
                'background-color: '. $atts['anchor_c'] .';'.
                'color: #fff; 
            }
           
            '
            
            
            
        );
    }
    add_action(
        'fw_ext_shortcodes_enqueue_static:icon_box',
        '_action_theme_shortcode_icon_box_enqueue_dynamic_css'
    );
    
    endif;