<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
/**
 * @var array $atts
 */



// fw_print(buildox_hex2rgba($atts['icon_color'],'0.1'));

$hexColor = buildox_hex2rgba($atts['icon_color'],'0.1');
/*
 * `.fw-iconbox` supports 3 styles:
 * `fw-iconbox-1`, `fw-iconbox-2` and `fw-iconbox-3`
 */
if($atts['style'] == 'value-1') : ?>
	<div class="service-section <?php echo esc_attr('shortcode-' . $atts['id']); ?>" >
	<div class="service-bordered-item">
		<span class="icon mb-30" style="color: <?php echo $atts['icon_color'];?>">
			<i class="<?php echo esc_attr($atts['icon']['icon-class']); ?>"></i>
		</span>
		<h3 class="service-title mb-30"><?php echo esc_html( $atts['title']); ?></h3>
		<p class="mb-30">
			<?php echo esc_html( $atts['content']); ?>
		</p>
		<a href="<?php echo esc_url( $atts['link_url']); ?>" class="details-btn">
			<?php echo esc_html( $atts['link_txt']); ?>
		</a>
	</div>

	

</div>
<?php elseif($atts['style'] == 'value-2'): ?>
	<div class="service-section <?php echo esc_attr('shortcode-' . $atts['id']); ?>" >
	<div class="service-grid-item text-center">
		<span class="icon mb-30" style="color:<?php echo esc_attr($atts['icon_color']); ?>;background-color:<?php echo esc_attr($hexColor); ?>;">

			<i class="<?php echo esc_attr($atts['icon']['icon-class']); ?>"></i>
		</span>
		<h3 class="service-title mb-15"><?php echo $atts['title']; ?></h3>
		<p class="mb-30">
			<?php echo $atts['content']; ?>
		</p>
		<a href="<?php echo esc_url( $atts['link_url']); ?>" class="details-btn"></a>
	</div>
	</div>

<?php else : ?>
	
<div class="feature-section <?php echo esc_attr('shortcode-' . $atts['id']); ?>">
	<div class="feature-item-2 clearfix">
		<span class="icon color-default-yellow">
			<i class="<?php echo esc_attr($atts['icon']['icon-class']); ?>"></i>
		</span>
		<div class="feature-content">
			<h3 class="feature-title mb-15"><?php echo $atts['title']; ?></h3>
			<p class="mb-30">
				<?php echo $atts['content']; ?>
			</p>
		<a href="<?php echo esc_url( $atts['link_url']); ?>" class="details-btn">
			<?php echo esc_html( $atts['link_txt']); ?>
		</a>
		</div>
	</div>
</div>

<?php endif; ?>
