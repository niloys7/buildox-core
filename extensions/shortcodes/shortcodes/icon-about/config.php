<?php if (!defined('FW')) die('Forbidden');

$cfg = array();

$cfg['page_builder'] = array(
	'title'         => __('About Us Icon', 'fw'),
	'description'   => __('Add an About Us Box', 'fw'),
	'tab'           => __('Theme Elements', 'fw'),
	'icon' => 'flaticon flaticon-star',
);