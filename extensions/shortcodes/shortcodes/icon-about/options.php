<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(

'id'    => array( 'type' => 'unique' ),
	'popup' => array(
    'type' => 'addable-popup',
    
    'label' => __('Icon', 'fw'),
    'desc'  => __('Add icon box', 'fw'),
    'template' => '{{- title }}',
    'popup-title' => null,
    'size' => 'small', // small, medium, large
    'limit' => 2, // limit the number of popup`s that can be added
    'add-button-text' => __('Add Icon', 'fw'),
    'sortable' => true,
    'popup-options' => array(
	    	
				
			'icon'    => array(
				'type'  => 'icon-v2',
				'label' => __('Choose an Icon', 'fw'),
			),
			'title'   => array(
				'type'  => 'text',
				'label' => __( 'Title of the Box', 'fw' ),
			 	'value' => 'Icon Title',
			),
			'content' => array(
				'type'  => 'textarea',
				'label' => __( 'Content', 'fw' ),
				'value' => 'Icon desired content : Lorem ipsum dolor sit amet, consectetu r adipisicing elit, sed do eiusmod temp incididunt ut labore et dolore. ',
				'desc'  => __( 'Enter the desired content', 'fw' ),
			),
			'icon_color' => array(
				    'type'  => 'color-picker',
				    'value' => '#ff4d1c',
				    
				    // palette colors array
				    'palettes' => array( '#f7c605', '#ff4d1c','#009326','#006cff','#9227ff','#0cff84'),
				    'label' => __('Icon Color', 'fw'),
				    
				),
	        
	    ),
	)
	);
	/*
	'tab2' => array(
		'title' => esc_html__('Extra options', 'fw'),
		'type' => 'tab',
		'options' => array(
	 		
		),
	),
	*/

	
