<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
/**
 * @var array $atts
 */

// fw_print($id);


// fw_print($atts['popup']);

/*
 * `.fw-iconbox` supports 3 styles:
 * `fw-iconbox-1`, `fw-iconbox-2` and `fw-iconbox-3`
 */
?>
<div class="manager-section ">
<div class="manager-content row">
	<?php foreach ($atts['popup'] as $icon) { 
	
		?>

	<div class="col-lg-6 col-md-6 col-sm-12 <?php echo esc_attr('shortcode-' . $atts['id']); ?>">
		<div class="future-plan-item">
			<span class="icon mb-30"><i style="color:<?php echo esc_html($icon['icon_color']); ?>" class="<?php echo esc_attr($icon['icon']['icon-class']); ?>"></i></span>
			<h3 class="title-text mb-15"><?php echo esc_html($icon['title']); ?></h3>
			<p class="mb-0">
				<?php echo esc_html($icon['content']); ?>
			</p>
		</div>
	</div>
	
	<?php }
	?>

</div>
</div>