<?php if (!defined('FW')) die( 'Forbidden' ); ?>
<?php $id = uniqid('shortcode-'); ?>
<?php
/*
 * the `.fw-tabs-container` div also supports
 * a `tabs-justified` class, that strethes the tabs
 * to the width of the whole container
 */
// fw_print($atts);
?>
<div class="funfact-section <?php echo esc_attr(' shortcode-' . $atts['id']); ?>">
	<div class="counter-item text-center">
		<div id="circle-<?php echo esc_attr($id); ?>" class="circle-progress mb-30">
			<strong class="percentage-text"></strong>
		</div>
		<small class="counter-title"><?php echo esc_html( $atts['title'] ); ?></small>
	</div>
</div>

<?php 
$value = '0.'.$atts['percentage'];

if($atts['percentage'] == '100'){
$value = '1';
}
 ?>
<script>
jQuery.noConflict();
jQuery(document).ready(function(){
	
 jQuery('#circle-<?php echo esc_attr($id); ?>').circleProgress({
    value:<?php echo esc_attr($value); ?>  ,
    
    fill: '<?php echo esc_attr( $atts['icon_color'] ); ?>'
  }).on('circle-animation-progress', function(event, progress) {
    jQuery(this).find('strong').html(Math.round(<?php echo esc_html( $atts['percentage'] ); ?> * progress) + '<i>%</i>');
  });

var ru = jQuery(".<?php echo esc_attr($id); ?>").closest('section'); // get top wrapper
console.log(ru);
jQuery(ru).one("mouseenter", function(e){
 
jQuery('#circle-<?php echo esc_attr($id); ?>').circleProgress({
    value:<?php echo esc_attr($value); ?>  ,
    
    fill: '<?php echo esc_attr( $atts['icon_color'] ); ?>'
  }).on('circle-animation-progress', function(event, progress) {
    jQuery(this).find('strong').html(Math.round(<?php echo esc_html( $atts['percentage'] ); ?> * progress) + '<i>%</i>');
  });

});

});	


</script>