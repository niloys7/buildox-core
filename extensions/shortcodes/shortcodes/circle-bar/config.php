<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array();

$cfg['page_builder'] = array(
	//'disable_correction' => true,
	'title'       => esc_html__( 'Circle progress', 'fw' ),
	'description' => esc_html__( 'Add a Circle progress', 'fw' ),
	'tab'         => esc_html__( 'Theme Elements', 'fw' ),
	
	'icon' => 'fa fa-circle-o-notch',
	 'title_template' => '{{- title }} <span class="cix-title-template">{{=o.percentage}} <br> {{- o.title}} </span>',
	
);
