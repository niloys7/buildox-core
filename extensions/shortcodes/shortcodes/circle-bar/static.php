<?php if (!defined('FW')) die('Forbidden');

// find the uri to the shortcode folder
$uri = fw_get_template_customizations_directory_uri('/extensions/shortcodes/shortcodes/circle-bar');
$uri = plugin_dir_url( __FILE__ );


wp_enqueue_script(
    'circle-progress',
    $uri . 'static/js/circle-progress.min.js' ,array('jquery'),null ,true
);



if (!function_exists('_action_theme_shortcode_circle_bar_enqueue_dynamic_css')):

    /**
     * @internal
     * @param array $data
     */
    function _action_theme_shortcode_circle_bar_enqueue_dynamic_css($data) {
        $shortcode = 'circle_bar';
        $atts = shortcode_parse_atts( $data['atts_string'] );
        $atts = fw_ext_shortcodes_decode_attr($atts, $shortcode, $data['post']->ID);
       

        wp_add_inline_style(
            'buildox-core',
            '.shortcode-'.$atts['id'].' .percentage-text { '.
                'color: '. $atts['num_color'] .';'.
            ' } 
            .shortcode-'. $atts['id'] .' .counter-item .counter-title { '.
                'color: '. $atts['title_color'] .';'.
                
            ' }
            '
            
            
            
        );
    }
    add_action(
        'fw_ext_shortcodes_enqueue_static:circle_bar',
        '_action_theme_shortcode_circle_bar_enqueue_dynamic_css'
    );
    
    endif;