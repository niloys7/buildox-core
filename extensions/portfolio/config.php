<?php
$cfg['image_sizes'] = array(
    'featured-image' => array(
        'width'  => 1170,
        'height' => 560,
        'crop'   => false
    ),
    'gallery-image'  => array(
        'width'  => 1170,
        'height' => 560,
        'crop'   => false
    )
);
$cfg['has-gallery'] = true;