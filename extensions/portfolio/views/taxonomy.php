<?php 

get_header();
?>
	<!-- casestudy-section - start
		================================================== -->
		<section id="casestudy-section" class="casestudy-section sec-ptb-140 clearfix">
			<div class="container">

				
					<div class="row">
						<?php
						if ( have_posts() ) :

							

							/* Start the Loop */
							while ( have_posts() ) :
								the_post(); ?>

								 <div class="col-md-4">
									<div class="item">
										<div class="casestudy-grid-item">

											<div class="casestudy-image image-container">
												<?php the_post_thumbnail( $size = 'buildox-portfolio-thumb', $attr = '' );?>
												<a href="<?php the_permalink();?>" class="plus-effect"></a>
											</div>

											<div class="casestudy-content">
												
												<?php 

													$terms = get_the_terms( $post->ID, 'fw-portfolio-category' );
													foreach ( $terms as $term ) {?>
													<a href="<?php echo esc_url(get_term_link( $term, $taxonomy = 'fw-portfolio-category' )); ?>" class="post-category">

													<?php echo esc_html( $term->name ); ?>
													<?php }
												?>
													
												</a>
												<a href="<?php the_permalink();?>" class="casestudy-title mb-30"><?php the_title();?></a>
												<p class="mb-15">
													<?php the_excerpt();?>
												</p>
												<a href="<?php the_permalink();?>" class="details-btn"><?php echo esc_html__( 'view case study', 'fw' );?></a>
											</div>

										</div>
									</div>
								</div>   
								
							<?php
							endwhile;

							//the_posts_navigation();

							buildox_pagination();

						else :

							get_template_part( 'template-parts/content', 'none' );

						endif;
						?>
						
						
						
						
					</div>
					
					
				
				
			</div>
		</section>
		<!-- casestudy-section - end
		================================================== -->


<?php
get_footer(); 
